
function menuItems (){
    let array = [
            {name:"Inventura",
            image: {path: "./assets/img/News.svg" , alt : "Opis slike"},
            link:"#Inventura"}, 
            {name:"Inventar",
            image:{path:"./assets/img/Box.svg" , alt : "Opis slike"}, 
            link:"#Inventar"},
            {name:"Prostorije", 
            image:{path:"./assets/img/Classroom.svg" , alt : "Opis slike"},
            link:"#Prosorije"},
            {name:"Zaposlenici", 
            image:{path:"./assets/img/Contacts.svg", alt : "Opis slike"},
            link:"#Zaposlenici"},
            {name:"Administracija", 
            image:{path:"./assets/img/Services.svg" , alt : "Opis slike"},
            link:"#Administracija"},
        ];
    let ul = document.getElementById("list1");
    for( let i=0; i<array.length; i++){
        let img =document.createElement("img");
        let li = document.createElement("li");
        let a = document.createElement("a");
        img.className = "pictures";
        a.className = "a1";
        li.className = "navigation1";
        li.setAttribute("link" ,array[i].link[i]);
        img.setAttribute("src", array[i].image.path);
        img.setAttribute("alt", array[i].image.alt);
        li.innerText = array[i].name;
        li.prepend(img);
        a.append(li);
        ul.append(a);
    }


}
(() => {
    menuItems ();
})();