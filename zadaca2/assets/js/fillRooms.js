let rooms = [
    {name:"Predavaonica 1",
    itemsCount:"Broj predmeta 50"}, 
   
    {name:"Predavaonica 2",
    itemsCount:"Broj predmeta 50"},

    {name:"Predavaonica 3", 
    itemsCount:"Broj predmeta 50"},
    
    {name:"Predavaonica 4", 
    itemsCount:"Broj predmeta 50"},
    
    {name:"Predavaonica 5", 
    itemsCount:"Broj predmeta 50"},
    
    {name:"Predavaonica 6", 
    itemsCount:"Broj predmeta 50"},

    {name:"Portirnica", 
    itemsCount:"Broj predmeta 50"},

    {name:"Referada", 
    itemsCount:"Broj predmeta 50"},
];
function navsecond() {
    let ul = document.getElementById("list2");
    for( let i=0; i<rooms.length; i++){
        let prvi =document.createElement("div");
        let drugi = document.createElement("div");
        let li = document.createElement("li");
        li.className = "block"
        prvi.className = "prvi";
        drugi.className = "drugi";
        prvi.innerText = rooms[i].name;
        drugi.innerText = rooms[i].itemsCount;
        li.append(prvi);
        li.append(drugi);
        ul.append(li);
    }
}
let flag = true;
function sort() {
    flag = !flag;
    if (flag) {
        rooms.sort((a, b) => {
            if (a.name == b.name) return 0;
            if (a.name < b.name) return -1;
            if (a.name > b.name) return 1;
        });
    }
    else {
        rooms.sort((a, b) => {
            if (a.name == b.name) return 0;
            if (a.name < b.name) return 1;
            if (a.name > b.name) return -1;
        });
    }
let img = document.getElementsByClassName("img");
    if (flag) {
        img[0].style.setProperty('transform', 'rotateZ(0deg)');
    }
    else {
        img[0].style.setProperty('transform', 'rotateZ(180deg)');
    }
    let stara = document.getElementsByClassName("block");
    for (let i = stara.length - 1; i >= 0; i--) {
        stara[i].parentNode.removeChild(stara[i]);
    }
    navsecond();
}

(() => {
    let sortnav = document.getElementsByClassName("sortnav");
    sortnav[0].addEventListener("click",sort);
    navsecond();
})();