let selectNodes = [];

function getParents(node) {
    let arr = [node];
    let parent = node.parentElement;
    while (parent != null) {
        arr.push(parent);
        parent = parent.parentElement;
    }
    return arr;
}

function getLCA(parents) {
    for (let j = 0; j < parents[1].length; j++) {
        for (let i = 0; i < parents[0].length; i++) {
            if (parents[0][i] === parents[1][j]) {
				return parents[0][i];
            }
        }
    }
    return null;
}
function printPath() {
    if (selectNodes.length == 2) {
        let parents = [getParents(selectNodes[0]), getParents(selectNodes[1])];

        let LCA = getLCA(parents);

        let spliced = [
            parents[0].splice(parents[0].indexOf(LCA)),
            parents[1].splice(parents[1].indexOf(LCA) + 1)
        ];
		let elems = [];
        for (let el of parents[0]) {
            elems.push(el.tagName);
        }
        for (let eli in parents[1]) {
            elems.push(parents[1][parents[1].length - eli - 1].tagName);
        }
        console.log(elems.join(' -> '));
    }
}

function addNode(node) {
    let pos = selectNodes.indexOf(node);

        selectNodes.push(node);

        printPath();
    }
window.onclick = (event) => {
    let target = event.target;
    addNode(target);
}

