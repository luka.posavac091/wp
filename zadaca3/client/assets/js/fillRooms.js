// let rooms = [
//     {name:"Predavaonica 1",
//     itemsCount:"Broj predmeta 50"}, 

//     {name:"Predavaonica 2",
//     itemsCount:"Broj predmeta 50"},

//     {name:"Predavaonica 3", 
//     itemsCount:"Broj predmeta 50"},

//     {name:"Predavaonica 4", 
//     itemsCount:"Broj predmeta 50"},

//     {name:"Predavaonica 5", 
//     itemsCount:"Broj predmeta 50"},

//     {name:"Predavaonica 6", 
//     itemsCount:"Broj predmeta 50"},

//     {name:"Portirnica", 
//     itemsCount:"Broj predmeta 50"},

//     {name:"Referada", 
//     itemsCount:"Broj predmeta 50"},
// ];
let lastclicked;
let rooms = [];
let table = [];
function filltable(table, id) {
    let tbody = document.getElementById("tableelements");
    tbody.innerHTML = '';
    for (let i = 0; i < table.length; i++) {
        let tr = document.createElement("tr");
        let td1 = document.createElement("td");
        let td2 = document.createElement("td");
        let td3 = document.createElement("td");
        let td4 = document.createElement("td");
        let td5 = document.createElement("td");
        let img = document.createElement("img");
        let input = document.createElement("input")
        img.src = "./assets/img/Edit.svg";
        img.style.width = "20px";
        img.style.height = "20px";
        input.type = "checkbox"
        td1.innerText = table[i].name;
        td2.innerText = table[i].expected;
        td3.innerText = table[i].real;
        td4.append(img);
        td5.append(input);
        tr.append(td1, td2, td3, td4, td5);
        tbody.append(tr);

    }
};


function navsecond() {
    let ul = document.getElementById("list2");
    let title = document.getElementsByClassName("title");
    for (let i = 0; i < rooms.length; i++) {
        let prvi = document.createElement("div");
        let drugi = document.createElement("div");
        let li = document.createElement("li");
        li.className = "block"
        prvi.className = "prvi";
        drugi.className = "drugi";
        prvi.innerText = rooms[i].name;
        drugi.innerText = `Broj predmeta:${rooms[i].itemsCount}`;
        li.onclick = () => {
            lastclicked.style.backgroundColor = "#447cac";
            title[0].innerText = rooms[i].name;
            getrooms(rooms[i].id);
            li.style.backgroundColor = "#0892d0";
            lastclicked = li;
        }
        li.append(prvi);
        li.append(drugi);
        ul.append(li);
    }
    title[0].innerText = rooms[0].name;
    getrooms(rooms[0].id);
    lastclicked = ul.children[2];
    lastclicked.style.backgroundColor = "#447cac";
}
let flag = true;
function sort() {
    flag = !flag;
    if (flag) {
        rooms.sort((a, b) => {
            if (a.name == b.name) return 0;
            if (a.name < b.name) return -1;
            if (a.name > b.name) return 1;
        });
    }
    else {
        rooms.sort((a, b) => {
            if (a.name == b.name) return 0;
            if (a.name < b.name) return 1;
            if (a.name > b.name) return -1;
        });
    }
    let img = document.getElementsByClassName("img");
    if (flag) {
        img[0].style.setProperty('transform', 'rotateZ(0deg)');
    }
    else {
        img[0].style.setProperty('transform', 'rotateZ(180deg)');
    }
    let stara = document.getElementsByClassName("block");
    for (let i = stara.length - 1; i >= 0; i--) {
        stara[i].parentNode.removeChild(stara[i]);
    }
    navsecond();
}

const getrooms = (id) => {
    $.get("/rooms/" + id, (data) => {
        table = data;
        filltable(table, id);
    })
};
(() => {
    let sortnav = document.getElementsByClassName("sortnav");
    sortnav[0].addEventListener("click", sort);
    $.get("/rooms", (data) => {
        rooms = data;
        navsecond();
    });

})();