// const { createId } = require('./utility.js')
import { createId } from "./utility.js";
let rooms = [
    { name: "Predavaonica 1", itemsCount: 3, id: createId() },
    { name: "Predavaonica 2", itemsCount: 2, id: createId() },
    { name: "Predavaonica 3", itemsCount: 3, id: createId() },
    { name: "Portirnica", itemsCount: 1, id: createId() },
    { name: "Referada", itemsCount: 4, id: createId() }
];
let table = [
    {
        roomId: rooms[0].id,
        items: [
            { name: "Stolica", expected: 45, real: 40 },
            { name: "Ploča", expected: 2, real: 3 },
            { name: "Grb", expected: 1, real: 1 }
        ]
    },
    {
        roomId: rooms[1].id,
        items: [
            { name: "Računalo", expected: 10, real: 10 },
            { name: "Ploča", expected: 1, real: 1 }
        ]
    },
    {
        roomId: rooms[2].id,
        items: [
            { name: "Stol", expected: 2, real: 1 },
            { name: "Računalo", expected: 10, real: 10 },
            { name: "Ploča", expected: 1, real: 1 }
        ]
    },
    {
        roomId: rooms[3].id,
        items: [
            { name: "Stolica", expected: 2, real: 1 }
        ]
    },
    {
        roomId: rooms[4].id,
        items: [
            { name: "Stolica", expected: 45, real: 40 },
            { name: "Ploča", expected: 2, real: 3 },
            { name: "Kuhalo", expected: 1, real: 1 },
            { name: "Espresso aparat", expected: 1, real: 1 }
        ]
    }
];
export { rooms, table };
// exports.rooms = rooms;
// exports.table = table;