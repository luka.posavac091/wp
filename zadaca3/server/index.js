const { application } = require("express");
const express = require("express");
// import express from 'express';
const app = express();
const port = 3000;
const data = require('./data.js');
// import { rooms, table } from './data.js';

app.use(express.static("../client/"));

app.get("/", (req, res) => {
    res.sendFile("../client/index.html");
});


app.get("/rooms", (req, res) => {
    res.send(rooms);
});


app.get("/rooms/:id", (req, res) => {
    let items = [];
    for (let el of table) {
        if (el.roomId == (req.params.id))
            items = el.items;
    }
    res.send(items);
});


app.listen(port, () => {
    console.log(`I'm listening at ${port}`);
});









