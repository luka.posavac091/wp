function createId() {
    const { v4: uuidv4 } = require('uuid');
    return uuidv4();
};

exports.createId = createId;

// import { v4 as uuidv4 } from "uuid";

// export function createId() {
//     return uuidv4();
// }
