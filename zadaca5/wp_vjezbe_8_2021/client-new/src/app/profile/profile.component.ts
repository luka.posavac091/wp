import { Component, Input, OnInit } from '@angular/core';
import { Bio } from "../types";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  @Input() bio: Bio = {
    firstName: "", 
    lastName: "",
    url: "",
    about: ""
  }

  constructor() { }

  ngOnInit(): void {
  }

}
