export enum Page {
    profile, projects, about
};

export interface Bio {
    firstName: string, 
    lastName: string,
    url: string,
    about: string
};
export interface Project {
    name: string,
    description: string,
    id: number
}