const getBio = (callb) => {
    $.get("bio", (data) => {
        callb(data);
    });
}

const renderData = (data) => {
    const { firstName, lastName, url, about } = data.biography;
    $("#profile-name").text(`${firstName} ${lastName}`);
    $("#profile-about").text(`${about}`);

    

    $(".profile-container").first().css({
        "background": `url(./assets/imgs/${url})`,
        "background-position": "center",
        "background-size": "150%"
    });
}

$(() => {
    console.log("I'm using jQuery");
    getBio(renderData);

});