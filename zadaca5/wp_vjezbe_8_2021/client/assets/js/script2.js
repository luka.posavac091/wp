const getProjects = (callb) => {
    $.get("projects", (data) => {
        callb(data);
    });
}

const deleteProject = (id, callb) => {
    $.ajax({
        method: "DELETE",
        url: `projects/${id}`,
        success: callb
    });
}


let editProjectFlag = false;
let projectToEdit = null;
const editProject = (project, callb) => {
    editProjectFlag = true;
    projectToEdit = project;
    $("h3").first().text("Edit project");
    let form = $("form").first();
    let input = form.children().first();
    let textarea = form.children().eq(1);
    input.val(project.name);
    textarea.val(project.description);
}

const putProject = (project, callb) => {
    $.ajax({
        method: "put",
        data: project,
        url: `projects`,
        success: callb
    });
}


const postProject = (project, callb) => {
    $.post("projects", project, callb);
}

const renderData = (data) => {

    let h = $("<h3>Projects by far </h3>")
    let ul = $("<ul id='projects-list'><ul>");
    $("form").first().after(ul);
    $("form").first().after(h);
    
    const projects = data.projects;
    for (let project of projects) {
        let li = $(`<li></li>`);
        let text = $(`<span>${project.name}: ${project.description}</span>`);
        let btn = $(`<button class='remove'> &times; </button>`);

        let btn2 = $(`<button class='edit'> &#9998; </button>`);
        let btnDiv = $("<div></div>");
        btnDiv.append(btn2);
        btnDiv.append(btn);

        li.append(text);
        li.append(btnDiv);
        ul.append(li);
        btn.on("click", () => {
            deleteProject(project.id, (res) => {
                li.remove();
            });
        });

        btn2.on("click", () => {
            editProject(project, (res) => {

            });
        });
    }
} 

$(() => {
    getProjects(renderData);

    $("form").first().submit((event) => {
        let name = $(event.target).children().eq(0).val();
        let description = $(event.target).children().eq(1).val();
        let project = { name, description };
        if (editProjectFlag) {
            let form = $("form").first();
            let input = form.children().first();
            let textarea = form.children().eq(1);
            projectToEdit.name = input.val();
            projectToEdit.description = textarea.val();
            putProject(projectToEdit, (res) => {
                console.log(res);
               
            });
        } else {
            postProject(project, (res) => {
                console.log(res);
            });
        }

    });
});