const express = require("express");
const app = express();
const port = 3000;

const staticRoot = "../client/";

app.use(express.static(staticRoot));

app.get("/", (req, res) => {
    res.sendFile(`${staticRoot}index.html`);
});


app.get("/bio", (req, res) => {
    const biography = {
        firstName: "Christoph",
        lastName: "Waltz",
        url: "christoph_waltz.jpg",
        about: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure fugiat, dolor voluptates sapiente voluptatem autem officiis impedit dolorum est laudantium culpa sed illum earum praesentium distinctio aliquid perspiciatis quisquam. Sit quibusdam blanditiis quidem natus minus fugiat repellat deleniti itaque ipsam excepturi, dignissimos cupiditate placeat porro, inventore ullam expedita, qui rem modi amet! Hic quae impedit, iusto, officiis expedita nisi libero minima in optio fugiat delectus harum dolorem? Sint a ab obcaecati corrupti illo unde quo quaerat voluptas repudiandae nemo minima deleniti hic consequatur aliquid voluptates veniam, similique incidunt iure in mollitia voluptatum facere? Nam architecto eos, vel impedit cupiditate quod tenetur, ipsum incidunt voluptas, porro quidem nemo enim aperiam facilis ullam expedita itaque officiis similique sequi soluta vero saepe. Vel voluptas dolor nulla illum commodi laborum sapiente, atque blanditiis? Reprehenderit aspernatur aliquid, aut laboriosam deleniti veritatis temporibus autem at illum, nisi sapiente nobis, veniam ut amet. Cumque doloribus perspiciatis accusamus! Sint, magni, asperiores nesciunt debitis dolorem eius aliquid nam dolorum mollitia aperiam nulla eligendi vero ex quis ad facilis, quisquam impedit. Numquam, possimus esse. Quos aut eos maiores, vel ex recusandae earum cupiditate similique eum perspiciatis suscipit. Ad alias, laudantium obcaecati saepe doloribus eum animi totam labore repudiandae amet consectetur, eius, reprehenderit temporibus placeat fugit. Optio facilis sed exercitationem soluta iure, commodi at iusto, hic id rerum neque libero autem, quas aperiam earum asperiores excepturi quae. Quasi quis atque nemo esse accusantium ducimus reprehenderit nobis ipsam! Nemo earum maxime ratione nesciunt expedita aspernatur nam dicta velit beatae? Recusandae molestiae nobis sed dolore! Neque libero quod exercitationem quisquam nisi, quos cupiditate omnis quam et laborum repellat at iure nihil aut delectus ea dicta? Dolorem cupiditate deleniti praesentium hic laboriosam repudiandae suscipit dolore aperiam, perferendis sequi quod molestias possimus officia sunt tempore laborum soluta doloremque vitae consequuntur molestiae ut distinctio expedita. Incidunt?"
    };
    const message = "Successfully retrieved";
    res.send({ biography, message });
});

app.listen(port, () => {
    console.log(`Listening at ${ port }`);
});


let projects = [
    { name: "Django Unchained", description: "Christoph's second blockbuster", id: 0 },
    { name: "Inglorious Basterds", description: "Brad Pitt is main character", id: 1 }
];
let nextId = 2;

app.get("/projects", (req, res) => {
    res.send({ projects });
});

app.delete("/projects/:id", (req, res) => {
    const id = req.params.id;
    projects = projects.filter((item) => item.id != id);
    res.send({ message: "Successfully removed" });
});


app.use(express.json()); //For JSON requests
app.use(express.urlencoded({extended: true}));
app.post("/projects", (req, res) => {
    projects.push({ ...req.body, id: ++nextId});
    res.send({ message: "Successfully added" });
});


app.put("/projects", (req, res) => {
    const id = req.body.id;
    for (let i_p in projects) {
        if (projects[i_p].id == parseInt(id)) {
            projects[i_p].name = req.body.name;
            projects[i_p].description = req.body.description;
            break
        }
    }
    res.send({ message: "Successfully updated" });
});