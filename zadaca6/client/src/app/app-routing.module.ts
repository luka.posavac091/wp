import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { ProfileComponent } from './profile/profile.component';
import { ProjectFormComponentComponent } from './project-form-component/project-form-component.component';
import { ProjectsComponent } from './projects/projects.component';

const routes: Routes = [{ path: 'profile', component: ProfileComponent },
{ path: 'projects', component: ProjectsComponent },
{ path: 'about', component: AboutComponent },
{ path: 'projects/form', component: ProjectFormComponentComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
