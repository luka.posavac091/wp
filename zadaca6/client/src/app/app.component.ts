import { Component, OnDestroy } from '@angular/core';
import { find } from 'rxjs';

import { Page, Bio, Project } from "./types";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
  pages: any = Page;
  bio: Bio = {
    firstName: "Christoph",
    lastName: "Waltz",
    url: "christoph_waltz.jpg",
    about: "Lorem ipsum dolor"
  };
  nextid: number = 2
  projects: Project[] = [
    { name: "Django Unchained", description: "Christoph's second blockbuster", id: 0 },
    { name: "Inglorious Basterds", description: "Brad Pitt is main character", id: 1 }
  ];
  selectedPage: Page = this.pages.profile;

  sub(projekt: Project) {
    this.projects.push(projekt);
    this.nextid++;
  }

  edit(editedProject: Project) {
    let i = this.projects.findIndex((project) =>
      project.id == editedProject.id)
    this.projects[i] = {
      name: editedProject.name,
      description: editedProject.description,
      id: editedProject.id
    }
  }
  remove(removeProject: Project) {
    this.projects = this.projects.filter((project) =>
      project.id != removeProject.id)
  }


  ngOnInit(): void {
  }
  ngOnDestroy(): void {

  }
}
