import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { FooterComponent } from './footer/footer.component';
import { ProfileComponent } from './profile/profile.component';
import { ProjectsComponent } from './projects/projects.component';
import { AboutComponent } from './about/about.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ProjectFormComponentComponent } from './project-form-component/project-form-component.component';
import { AppRoutingModule } from './app-routing.module';
import { ProjectsServiceService } from './projects/projects-service.service';
import { ProfileServiceService } from './profile/profile-service.service';
import { HttpClientModule } from "@angular/common/http";


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FooterComponent,
    ProfileComponent,
    ProjectsComponent,
    AboutComponent,
    ProjectFormComponentComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [ProjectsServiceService, ProfileServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
