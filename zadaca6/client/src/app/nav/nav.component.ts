import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { Page } from "../types";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})


export class NavComponent implements OnInit {

  pages: any = Page;
  selectedPage: Page = Page.profile;
  @Output() pageChanged: EventEmitter<Page>;
  constructor() {
    this.pageChanged = new EventEmitter<Page>();
  }

  ngOnInit(): void {
  }

  selectPage(page: Page) {
    this.selectedPage = page;
    this.pageChanged.emit(page);
  }

}
