import { HttpClient } from '@angular/common/http';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Bio } from '../types';
@Injectable({
  providedIn: 'root'
})
export class ProfileServiceService {
  message: string = "";
  biography: Bio = {
    firstName: "",
    lastName: "",
    url: "",
    about: ""
  };
  constructor(
    public httpClient: HttpClient
  ) {
  }
  // this.getBiography();
  getData(): Observable<{ biography: Bio, message: Message }> {
    return this.httpClient.get<{ biography: Bio, message: Message }>("/api/bio")
  }
  profileBS: BehaviorSubject<{ biography: Bio, message: string }> = new BehaviorSubject<{ biography: Bio, message: string }>({ biography: this.biography, message: this.message });
  getBiography(): Observable<{ biography: Bio, message: string }> {
    return this.profileBS
  }

}
