import { Component, Input, OnInit } from '@angular/core';
import { Bio } from "../types";
import { ProfileServiceService } from './profile-service.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  bio: Bio = {
    firstName: "",
    lastName: "",
    url: "",
    about: ""
  }

  constructor(public PS: ProfileServiceService) {
  }

  data: { bio: Bio, message: string } = {
    bio: {
      firstName: "",
      lastName: "",
      url: "",
      about: ""
    }, message: ""
  }

  ngOnInit(): void {
    // this.PS.getBiography().subscribe(element => { this.bio = element.biography });
    // console.log(this.bio);
    console.log('hello')
    this.PS.getData().subscribe(data => {
      this.bio = data.biography;
      console.log('hello1')
    })
  }
}
