import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, NgForm } from '@angular/forms';
import { Project } from "../types";
import { Location } from '@angular/common';

@Component({
  selector: 'app-project-form-component',
  templateUrl: './project-form-component.component.html',
  styleUrls: ['./project-form-component.component.css']
})
export class ProjectFormComponentComponent implements OnInit {
  f: FormGroup
  constructor(public location: Location) {
    this.f = new FormGroup({
      name: new FormControl(),
      discription: new FormControl()
    });
  }
  backPressed(): void {
    this.location.back();
  }

  ngOnInit(): void {
  }
  submitForm() {
    let v: { name: string, discription: string } = this.f.value
  }

}
