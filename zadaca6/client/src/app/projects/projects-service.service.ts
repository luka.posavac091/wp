import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, elementAt, Observable } from 'rxjs';
import { Project } from '../types';
@Injectable({
  providedIn: 'root'
})
export class ProjectsServiceService {
  message: string = "Hello2"
  projekt: Project[] = [
    { name: "Django Unchained", description: "Christoph's second blockbuster", id: 0 },
    { name: "Inglorious Basterds", description: "Brad Pitt is main character", id: 1 }
  ];
  idelement = 0
  project: Project = { name: "", description: "", id: 0 }
  constructor(
    public httpClient: HttpClient
  ) {
    this.getData().subscribe(element => { this.project = element.projects[0] });
    this.getId().subscribe(element => { this.idelement = element.})
  }
  getData(): Observable<{ projects: Project[], message: string }> {
    return this.httpClient.get<{ projects: Project[], message: string }>("/api/projects");
  }

  getId(): Observable<{ message: string, project: Project[] }> {
    return this.httpClient.get<{ message: string, project: Project[] }>("/api/projects/:id");
  }

  projectBS: BehaviorSubject<{ message: string, projects: Project[] }> = new BehaviorSubject<{ message: string, projects: Project[] }>({ message: this.message, projects: this.projekt });
  messageBS: BehaviorSubject<{ message: string }> = new BehaviorSubject<{ message: string }>({ message: this.message });

  getProjects(): Observable<{ projects: Project[] }> {
    return this.projectBS;
  }
  remove(project: Project): Observable<{ message: string }> {
    return this.messageBS;
  }
  onSubmit(project: Project): Observable<{
    message: string, projects: Project[]
  }> {
    return this.projectBS
  }
  edit(project: Project): Observable<{
    message: string, projects: Project[]
  }> {
    return this.projectBS;
  }
}
