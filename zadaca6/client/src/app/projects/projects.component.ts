import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Project } from "../types";
import { ProjectsServiceService } from './projects-service.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent {
  projects: Project[] = [];
  nextid: number = 0;
  projekt: Project = { name: "", description: "", id: 0 };
  Addlist = new EventEmitter<Project>();
  editProject = new EventEmitter<Project>();
  removeProject = new EventEmitter<Project>();

  editingFlag: boolean = false;

  onSubmit(forms: NgForm): void {
    if (forms.form.value.name == "" || forms.form.value.description == "") {
    }
    else {
      if (this.editingFlag) {
        let value: Project = forms.form.value;
        this.editProject.emit(value);
      }
      else {
        let value: Project = forms.form.value;
        value.id = this.nextid;
        this.Addlist.emit(value);
      }
    }
    this.editingFlag = false;
    this.projekt.name = "";
    this.projekt.description = "";
    this.projekt.id = 0;
  }

  edit(editprojekt: Project) {
    this.editingFlag = true;
    this.projekt.name = editprojekt.name;
    this.projekt.description = editprojekt.description;
    this.projekt.id = editprojekt.id;
  }

  remove(removed: Project) {
    this.removeProject.emit(removed);
  }


  constructor(public PJ: ProjectsServiceService) {
  }

  ngOnInit(): void {
    this.PJ.getData().subscribe(element => { this.projects = element.projects });
  }
}
