exports.biography = {
    firstName: "Christoph",
    lastName: "Waltz",
    url: "christoph_waltz.jpg",
    about: "a vestibulum mi volutpat vel.Nullam ullamcorper ex leo, eget bibendum urna volutpat id.Vivamus commodo sem velcondimentum aliquam.Sed odio nulla, vulputate id velit volutpat, tincidunt blandit felis.Suspendisse tinciduntdiam tellus, quis dignissim diam laoreet sed.Duis nec justo ipsum.Cras pharetra bibendum nulla vel aliquam.Duis quis eros dui.Pellentesque vel turpis at ex suscipit finibus.Duis consectetur convallis nulla in facilisis.Nullam eu tempus quam.Duis luctus arcu id justo luctus, id finibus magna laoreet.Fusce accumsan vitae purus atiaculis.Morbi vel nunc lorem.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpisegestas.Maecenas nec nibh massa.Maecenas in ante suscipit, cursus neque non, dapibus nibh.Duis nisl metus, dictumut tempus lacinia, tempor et erat. "
};
exports.projekt = [
    { name: "Django Unchained", description: "Christoph's second blockbuster", id: 0 },
    { name: "Inglorious Basterds", description: "Brad Pitt is main character", id: 1 }
];
