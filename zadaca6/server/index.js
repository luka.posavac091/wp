const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const port = 3000;
const { createId } = require("./utils");
const { biography, projekt } = require('./data.js')

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.listen(port, () => {
    console.log(`I am listening on ${port}`)
});
//BIO
app.get("/api/bio", (req, res) => {
    const message = "Successfully retrieved";
    console.log(message);
    res.send({ biography, message });
});
// PROJECTS
app.get("/api/projects", (req, res) => {
    res.send({ projekt });
});
app.delete("/api/projects/:id", (req, res) => {
    const id = req.params.id;
    projekt = projekt.filter((item) => item.id != id);
    res.send({ message: "Successfully removed" });
});
app.post("/api/projects", (req, res) => {
    projekt.push({ ...req.body, id: createId() });
    let project = projekt[projekt.length - 1];
    res.send({ message: "Successfully added", projekt });
});
app.put("/api/projects", (req, res) => {
    let project = undefined;
    for (let i_p in projekt) {
        if (projekt[i_p].id == req.body.id) {
            projekt[i_p].name = req.body.name;
            projekt[i_p].description = req.body.description;
            project = projekt[i_p];
            break
        }
    }
    res.send({ message: "Successfully updated", project });
});