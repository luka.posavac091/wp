
function createId() {
    const { v4: uuidv4 } = require('uuid');
    return uuidv4();
};

exports.createId = createId;